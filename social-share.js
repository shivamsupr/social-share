/*************************** Social Share ******************************/

function SocialShare() {
}

var SSJ = new SocialShare();

SocialShare.prototype.handleSocialShare = function ($element) {
    $element.on('click', function () {
        var $self = $(this);
        var share_title = $self.data('title');
        var share_url = $self.data('url');

        if ($('.sclShr').hasClass('open')) {
            $('.sclShrDrpdwn', $('.sclShr.open')).remove();
        }

        $self.append(SSJ.getSocialShareDropdown(getShareContent(share_title, share_url), share_url)).addClass('open');
        $('.sclShrDrpdwn', $self).css({top: $self.offset().top + 35}).removeClass('hide');
    });

    $('body').on('click', function (event) {
        if (!$(event.target).hasClass('sclShr')) {
            $('.sclShr').removeClass('open');
            $('.sclShrDrpdwn').remove();
        }
    });

    function getShareContent(share_title, share_url) {
        var share_content = '';
        if (share_title !== undefined) {
            share_content += share_title + "%20";
        }
        share_content += share_url;

        return share_content;
    }
};

SocialShare.prototype.getSocialShareDropdown = function (share_content, share_url) {
    return '\
    <ul class="scl-shr-drpdwn text-left sclShrDrpdwn hide"> \
        <li class="scl-shr-drpdwn--itm np"> \
            <a href="https://www.facebook.com/sharer/sharer.php?u=' + share_url + '" target="_blank"> \
                <i class="fa fa-facebook-square scl-shr-drpdwn__icn fb"></i>&nbsp;<span class="scl-shr-drpdwn__txt">Facebook</span> \
            </a> \
        </li> \
        <li class="scl-shr-drpdwn--itm np"> \
            <a href="https://twitter.com/home?status=' + share_content + '" id="usrLogout" target="_blank"> \
                <i class="fa fa-twitter-square scl-shr-drpdwn__icn twttr"></i>&nbsp;<span class="scl-shr-drpdwn__txt">Twitter</span> \
            </a> \
        </li> \
        <li class="scl-shr-drpdwn--itm"> \
            <a href="https://plus.google.com/share?url=' + share_url + '" target="_blank"> \
                <i class="fa fa-google-plus-square scl-shr-drpdwn__icn gplus"></i>&nbsp;<span class="scl-shr-drpdwn__txt">Google Plus</span> \
            </a> \
        </li>\
    </ul> \
      ';
};
